'use strict';

const { DOMParser } = require('xmldom');
const { SignedXml, xpath } = require('xml-crypto');

function MyKeyInfo(content) {
  this.getKeyInfo = function() {
    return `<X509Data><X509Certificate>${content
      .replace('-----BEGIN CERTIFICATE-----', '')
      .replace('-----END CERTIFICATE-----', '')
      .replace(/(\r\n\t|\n|\r\t)/gm, '')}</X509Certificate></X509Data>`.replace(/\s/g, '');
  };
}

/**
 * @description Signature service.
 * @param {String} xml Invoice under xml format.
 * @param {String} key Company private key.
 * @param {String} crt Company public key.
 * @returns {String} Signed xml.
 */
exports.signature = (xml, key, crt) => {
  return new Promise((resolve, reject) => {
    const signedXml = new SignedXml();
    signedXml.addReference(
      '//*[local-name(.)="infNFe"]',
      ['http://www.w3.org/2000/09/xmldsig#enveloped-signature', 'http://www.w3.org/TR/2001/REC-xml-c14n-20010315'],
      ['http://www.w3.org/2000/09/xmldsig#sha1']
    );
    signedXml.signingKey = key;
    signedXml.canonicalizationAlgorithm = 'http://www.w3.org/TR/2001/REC-xml-c14n-20010315';

    // Public key provider.
    signedXml.keyInfoProvider = new MyKeyInfo(crt);

    signedXml.computeSignature(xml, {
      location: {
        reference: '//*[local-name(.)="infNFe"]',
        action: 'after'
      }
    });
    const res = signedXml.getSignedXml();
    if (!res) reject('Signature process has been failed');
    else resolve(res);
  });
};

exports.check = (xml, crt) => {
  return new Promise((resolve, reject) => {
    const signedXml = new SignedXml();
    const doc = new DOMParser().parseFromString(xml);

    const signature = xpath(
      doc,
      '//*[local-name(.)="Signature" and namespace-uri(.)="http://www.w3.org/2000/09/xmldsig#"]'
    )[0];

    signedXml.keyInfoProvider = {
      getKey: function() {
        return crt;
      }
    };
    signedXml.loadSignature(signature);
    const res = signedXml.checkSignature(xml);
    res ? resolve(res) : reject(res);
  });
};
