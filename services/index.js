'use strict';

module.exports = {
  signature: require('./signature').signature,
  check: require('./signature').check
};
