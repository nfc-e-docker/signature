'use strict';

require('dotenv').config();
const grpc = require('grpc');
const path = require('path');
const certs = require('./certs');
const { timeout } = require('./lib');
const protoLoader = require('@grpc/proto-loader');
const { signature, check } = require('./services');

module.exports.init = async () => {
  // Proto loading
  const packageDefinition = await protoLoader.load(path.join(__dirname, 'protos', 'signature.proto'));
  const SignatureService = grpc.loadPackageDefinition(packageDefinition).SignatureService;
  const server = new grpc.Server();

  server.addService(SignatureService.service, {
    signature: async (call, callback) => {
      try {
        const { xml, key, crt } = call.request;
        const signedXml = await signature(xml, key, crt);
        callback(null, { xml: signedXml });
      } catch (err) {
        callback(err);
      }
    },
    check: async (call, callback) => {
      try {
        const { xml, crt } = call.request;
        const checkResult = await check(xml, crt);
        callback(null, { checkResult });
      } catch (err) {
        callback(err);
      }
    }
  });
  server.bind(
    `${process.env.GRPC_HOST}:${process.env.GRPC_PORT}`,
    grpc.ServerCredentials.createSsl(certs.rootCerts, certs.keyCertPairs, true)
  );
  server.start();

  if (process.env.TEST_TYPE === 'integrated') {
    // Registers at gateway.
    timeout.connectToGateway((err, _res) => {
      if (err) {
        // Gracefully shuts down the server. The server will stop receiving new calls, and any pending calls will complete.
        server.tryShutdown(() => {
          process.exit(1);
        });
      }
      // Checks gateway heartbeat.
      timeout.heartbeat();
    });
  }
};
