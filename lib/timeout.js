'use strict';

const createClient = require('../lib/client');

exports.connectToGateway = async callback => {
  let attempts = 0;
  const client = await createClient(process.env.GATEWAY_HOST, process.env.GATEWAY_PORT, 'RegistryService');
  if (!client) callback('Client was not created');
  // Registers at gateway.
  const time = setInterval(() => {
    client.register({ name: 'signature', ipv4: process.env.GRPC_HOST, port: process.env.GRPC_PORT }, err => {
      if (err) {
        // eslint-disable-next-line no-console
        console.info('\x1b[33m', '\tTrying to reconnect to the gateway...');
        attempts++;
      }
      if (err && attempts == process.env.WAIT_FOR_GATEWAY_ATTEMPTS) {
        // eslint-disable-next-line no-console
        console.info('\x1b[31m', '\tFailed to connect to the gateway');
        callback(err);
        clearInterval(time);
      }
      if (!err) {
        clearInterval(time);
        callback(null);
      }
    });
  }, process.env.WAIT_FOR_GATEWAY);
};

exports.heartbeat = async () => {
  let attempts = 0;
  const client = await createClient(process.env.GATEWAY_HOST, process.env.GATEWAY_PORT, 'RegistryService');
  if (!client) {
    // eslint-disable-next-line no-console
    console.info('\x1b[31m', '\tClient was not created');
    process.exit(1);
  }
  const time = setInterval(() => {
    client.echo({ ping: 'ping' }, (err, _res) => {
      if (err) {
        attempts++;
        // eslint-disable-next-line no-console
        console.info('\x1b[31m', '\tTrying to check gateway heartbeat...');
        this.connectToGateway(err => {
          if (err) process.exit(1);
        });
      }
      if (err && attempts == process.env.GATEWAY_HEARTBEAT_ATTEMPTS) {
        clearInterval(time);
        // eslint-disable-next-line no-console
        console.info('\x1b[31m', '\tNo gateway available. Service died.');
        process.exit(1);
      }
      if (!err) {
        attempts = 0;
      }
    });
  }, process.env.GATEWAY_HEARTBEAT);
};
