'use strict';

require('dotenv').config();
const path = require('path');
const certs = require('../certs');
const server = require('../server');
const { readFileSync } = require('fs');
const { should, expect } = require('chai');
const createClient = require('../lib/client');

let client = null;
let xml = null;
let signedXml = null;

describe('Module: Signature', () => {
  before(done => {
    // Starts the service.
    server
      .init()
      .then(() => {
        createClient(process.env.GRPC_HOST, process.env.GRPC_PORT, 'SignatureService')
          .then(_client => {
            client = _client;
            // Invoice loading.
            xml = readFileSync(path.join(__dirname, 'xml', 'unsigned.xml'), { encoding: 'utf-8' });
            done();
          })
          .catch(err => {
            done(err);
          });
      })
      .catch(err => {
        done(err);
      });
  });

  it('Method: Signature', done => {
    client.signature(
      { xml, key: certs.keyCertPairs[0].private_key, crt: certs.keyCertPairs[0].cert_chain },
      (err, res) => {
        should().not.exist(err);
        should().exist(res.xml);
        expect(res.xml).to.be.contain('SignatureValue');
        signedXml = res.xml;
        done();
      }
    );
  });

  it('Method: Check signature', done => {
    client.check({ xml: signedXml, crt: certs.keyCertPairs[0].cert_chain }, (err, res) => {
      should().not.exist(err);
      should().exist(res.checkResult);
      expect(res.checkResult).to.be.equal(true);
      done();
    });
  });
});
